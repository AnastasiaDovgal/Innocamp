import React, { useState } from "react";
import FilmCart from "./components/FilmCart";
import './App.css';

function App() {
  const [items, setItems] = useState([]);

  const fetchData = event => {
    event.preventDefault();

    let title = event.target[0].value;
    let year = event.target[1].value;
    if (year) {
      title = "http://www.omdbapi.com/?apikey=4b601aab&s=*" + title + "*&y=" + year + "&type=movie&page=1";
    } else {
      title = "http://www.omdbapi.com/?apikey=4b601aab&s=*" + title + "*&type=movie&page=1";
    }
    fetch(title)
      .then(response => response.json())
      .then(commits => setItems(commits.Search));
  };

  return (
    <div>
      <form onSubmit={fetchData}>
        <input type="text" placeholder="name" />
        <input type="text" placeholder="year" />

        <button type="submit">Search</button>

        <div className="film-list">
          {items ? items.map(i => {
            return <FilmCart Title={i.Title} Year={i.Year} Img={i.Poster} />;
          }) : items === null}
        </div>
      </form>

      {/* <ul onClick={chanchPage} className="pagination"> */}
      <ul className="pagination">
        <li>Page 1</li>
        <li>Page 2</li>
      </ul>
    </div>
  );
}

export default App;