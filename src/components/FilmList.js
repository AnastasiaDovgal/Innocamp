import React, { useState } from "react";
import './FilmList.css';
import FilmCart from './FilmCart';

export default function FilmList() {
    // 20 ITEMS
    // 10 ITEMS  CAN
    // 2 PAGES

    // await(await fetch("http://www.omdbapi.com/?apikey=4b601aab&i=tt1078912")).json();

    const [items, setItems] = useState([
        {
          Title: "Test",
          Year: "2000"
        }
      ]);
    
    const fetchData = event => {
        event.preventDefault();

        let title = event.target[0].value;
        let year = event.target[1].value;

        title = "http://www.omdbapi.com/?apikey=4b601aab&s=*" + title + "*&type=movie&page=1";
        // fetch('http://www.omdbapi.com/?apikey=4b601aab&i=tt1078912')
        // fetch("http://www.omdbapi.com/?apikey=4b601aab&s=*react*&type=movie&page=1")
        fetch(title)
            .then(response => response.json())
            // .then(commits => console.log(commits));
            .then(commits => setItems(commits.Search));
    };

    const getData = commits => {
        let data = commits;
        console.log('data', data);
    }

    const chanchPage = (event) => {

    }

    return (
        <div>
            <header>
                {/* <input type="text" placeholder="Serch by name" onChange={} /> */}
                <input type="text" placeholder="Serch by name" />
                <select>
                    <option placeholder="Select value" hidden></option>
                    <option>1990</option>
                    <option></option>
                    <option>2019</option>
                </select>
            </header>

            <div className="film-list">
                {/* <button onClick={fetchData}>Fetch</button>
            <button onClick={getData}>Log</button> */}

                <form onSubmit={fetchData}>
                    <input type="text" placeholder="name" />
                    <input type="text" placeholder="year" />

                    <button type="submit">Search</button>

                    {items.map(i => {
                        return <FilmCart Title={i.Title} Year={i.Year} />;
                    })}
                </form>

                <ul onClick={chanchPage} className="pagination">
                    <li>Page 1</li>
                    <li>Page 2</li>
                </ul>
            </div>
        </div>
    );
}