import React from 'react';
import './FilmDetails.css';

export default function FilmDetails(props) {
    // MODAL

    const closeDetails = () => {
        let detal = document.getElementsByClassName("film-details-container")[0];
        detal.style.display = "none";

        console.log('FilmDetails closed', detal);
    }
    return (
        <div className="film-details-container">
            <div className="film-details">
                <h1>Film Details</h1>
                <ul>
                    {/* <li><span>Title: {}</span></li> */}
                    <li>Title: {}</li>
                    <li>Director: {}</li>
                    <li>Actors: {}</li>
                    <li>Released: {}</li>
                    <li>BoxOffice: {}</li>
                    <li>Country: {}</li>
                    <li>Genre: {}</li>
                    <li>Runtime: {}</li>
                    <li>Plot: {}</li>
                    <li>imdbRating: {}</li>
                    <li>imdbVotes: {}</li>
                    <li>Poster: {}</li>
                </ul>
                <button onClick={closeDetails}>Close</button>
            </div>
        </div>
    );
}

