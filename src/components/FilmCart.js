import React from 'react';
import './FilmCart.css';
import FilmDetails from './FilmDetails';

export default function FilmCart(props) {
    const showFilmDetails = () => {
        // document.getElementsByClassName("film-details-container")[0].style.display = "none" ? "flex" : "none";
        let filmDetal = document.getElementsByClassName("film-details-container")[0];
        // filmDetal.style.display = "none" ? "flex" : "flex" ? "none" : "flex";
        filmDetal.style.display = "flex";

        console.log('FilmDetails open!', filmDetal);
    }

    return (
        <div onClick={showFilmDetails} className="film-cart">
            <p><span>Title:</span> {props.Title}</p>
            <p><span>Year:</span> {props.Year}</p>
            <a>
                <img src={props.Img} alt="Poster"></img>
            </a>
            <FilmDetails
                showFilmDetails={showFilmDetails}
            />
        </div>
    );
}

